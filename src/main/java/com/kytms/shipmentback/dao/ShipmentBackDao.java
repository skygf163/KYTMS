package com.kytms.shipmentback.dao;

import com.kytms.core.dao.BaseDao;

/**
 * 辽宁捷畅物流有限公司 -信息技术中心
 * 臧英明
 *
 * @author
 * @create 2018-01-19
 */
public interface ShipmentBackDao<ShipmentBack> extends BaseDao<ShipmentBack> {
}
