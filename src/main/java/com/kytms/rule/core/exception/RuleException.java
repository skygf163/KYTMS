package com.kytms.rule.core.exception;/**
 * Created by nidaye on 2018/5/7.
 */

/**
 * 辽宁捷畅物流有限公司 -信息技术中心
 * 臧英明
 * 规则异常
 *
 * @author
 * @create 2018-05-07
 */
public class RuleException extends RuntimeException {
    public RuleException() {
    }

    public RuleException(String message) {
        super(message);
    }

    public RuleException(Throwable cause) {
        super(cause);
    }

    public RuleException(String message, Throwable cause) {
        super(message, cause);
    }
}
