package com.kytms.core.entity;

import com.alibaba.fastjson.annotation.JSONField;
import org.apache.log4j.Logger;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 辽宁捷畅物流有限公司 -信息技术中心
 * 模板台账明细
 * @author 陈小龙
 * @create 2018-04-09
 */
@Entity(name = "JC_TEMPLATE_LEDGER_DETAIL")
public class TemplateLedgerDetails extends BaseEntityNoCode implements Serializable {
    private double amount; //金额
    private double taxRate; //税率
    private double input; //税金
    private TemplateLedger templateLedger;//模板台账表
    private FeeType feeType;//费用类型

    private final Logger log = Logger.getLogger(TemplateLedgerDetails.class);//输出Log日志


    @ManyToOne(cascade={CascadeType.PERSIST })
    @JoinColumn(name = "JC_FEE_TYPE_ID")
    public FeeType getFeeType() {
        return feeType;
    }
    public void setFeeType(FeeType feeType) {
        this.feeType = feeType;
    }

    @JSONField(serialize=false)
    @ManyToOne(cascade={CascadeType.ALL })
    @JoinColumn(name = "JC_TEMPLATE_LEDGER_ID")
    public TemplateLedger getTemplateLedger() {
        return templateLedger;
    }
    public void setTemplateLedger(TemplateLedger templateLedger) {
        this.templateLedger = templateLedger;
    }

    @Column(name = "AMOUNT")
    public double getAmount() {
        return amount;
    }
    public void setAmount(double amount) {
        this.amount = amount;
    }

    @Column(name = "TAXRATE")
    public double getTaxRate() {
        return taxRate;
    }
    public void setTaxRate(double taxRate) {
        this.taxRate = taxRate;
    }

    @Column(name = "INPUT")
    public double getInput() {
        return input;
    }
    public void setInput(double input) {
        this.input = input;
    }
}
