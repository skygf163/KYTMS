package com.kytms.shipmentTemplate.action;

import com.alibaba.fastjson.JSONObject;
import com.kytms.core.action.BaseAction;
import com.kytms.core.entity.*;
import com.kytms.core.model.CommModel;
import com.kytms.core.model.JgGridListModel;
import com.kytms.core.model.ReturnModel;
import com.kytms.shipmentTemplate.service.TemplateService;
import com.kytms.shipmentback.service.Impl.ShipmentBackServiceImpl;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

/**
 * 辽宁捷畅物流有限公司 -信息技术中心
 * 运单模板控制层
 * @author 陈小龙
 * @create 2018-04-09
 */
@Controller
@RequestMapping("/template")
public class TemplateAction extends BaseAction {
    private final Logger log = Logger.getLogger(TemplateAction.class);//输出Log日志
    private TemplateService<ShipmentTemplate> templateService;
    @Resource
    public void setTemplateService(TemplateService<ShipmentTemplate> templateService) {
        this.templateService = templateService;
    }




    @RequestMapping(value = "/getTemplateList", produces = "text/json;charset=UTF-8")
    @ResponseBody
    public String  getList(CommModel commModel){
        JgGridListModel jgGridListModel =templateService.getList(commModel);
        return jgGridListModel.toJSONString();
    }

    @RequestMapping(value = "/saveTemplate", produces = "text/json;charset=UTF-8")
    @ResponseBody
    public String saveTemplate(String str){
        ReturnModel returnModel = getReturnModel();
        ShipmentTemplate shipmentTemplate= JSONObject.parseObject(str, ShipmentTemplate.class);
        //唯一性校驗
        returnModel.codeUniqueAndNull(shipmentTemplate,templateService);
        if (shipmentTemplate.getOperationPattern() == 0){
           //如果是自营 外部车牌号和外部司机清空
            shipmentTemplate.setCarrier(null);
            shipmentTemplate.setOutDriver(null);
            shipmentTemplate.setLiense(null);
        }else{//如果是外包 清空车头，司机，车挂，车厢
            shipmentTemplate.setVehicleHead(null);
            shipmentTemplate.setDriver(null);
        }
        List<TemplateLedger> ledgers = shipmentTemplate.getTemplateLedgers();
        for (TemplateLedger ledger:ledgers ) {
            if (shipmentTemplate.getOperationPattern() != 0) { //如果是外部 需要验证费用明细
                if (ledger.getTemplateLedgerDetails() == null || ledger.getTemplateLedgerDetails().size() == 0) {
                    returnModel.addError("code", "运单费用必须有一条明细");
                    break;
                }
            }
            List<TemplateLedgerDetails> ledgerDetails = ledger.getTemplateLedgerDetails();
            for (TemplateLedgerDetails ledgerDetail:ledgerDetails){
                double amount = ledgerDetail.getAmount();
                if (amount<0){
                    returnModel.addError(ledgerDetail.getFeeType().getId(),"费用不能是负数");
                }
            }
        }
        boolean result = returnModel.isResult();
        if (result){
            ShipmentTemplate shipmentTemplate1 =    templateService.saveTemplate(shipmentTemplate);
            returnModel.setObj(shipmentTemplate1);
        }
        return returnModel.toJsonString();
    }
}
